<?php

namespace App\Entity;

use App\Utils\StringParserTrait;
use DateTime;
use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
	use StringParserTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tweet_id;

	/** @ManyToMany(targetEntity="Tag", inversedBy="articles") */
	private $tags;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
	{
		$this->tags = new ArrayCollection();
	}

	public function getId()
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTweetId(): ?string
    {
        return $this->tweet_id;
    }

    public function setTweetId(string $tweet_id): self
    {
        $this->tweet_id = $tweet_id;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function addTag(Tag $tag){
    	$tag->addArticle($this);
    	$this->tags[] = $tag;
	}

	public function getTags(){
    	return $this->tags;
	}

	public function getDateTime() : DateTime{
    	$currentTimezone =  new DateTimeZone(getenv('TIMEZONE'));
    	$dateTime = new DateTime();
    	$dateTime->setTimestamp($this->getCreatedAt()->getTimestamp());
		$dateTime->setTimezone($currentTimezone);
    	return $dateTime;
	}

	public function displayText(string $query = '') : string
	{
		$body = strip_tags($this->getText());
		if(!empty($query)){
			$body = $this->hightlightString($query, $body);
		}
		$body = $this->parseUrl($body);
		return $body;
	}
}
