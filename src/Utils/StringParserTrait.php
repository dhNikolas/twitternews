<?php


namespace App\Utils;


trait StringParserTrait
{

	protected function hightlightString(string $string, $body) : string {
		return preg_replace(
			"/(".$string.")/iu",
			"<span style=\"color: #33cc00; background: yellow\">$1</span>"
			,$body);
	}

	protected function parseUrl(string $body) : string {
		return preg_replace(
			"/(((https|http)(:\/\/))[-a-zA-Z0-9@:%\/._\+~#=]{2,256})/im",
			"<a href='$1' target='_blank'>$1</a>"
			,$body);
	}

}