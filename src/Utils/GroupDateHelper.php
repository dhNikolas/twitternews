<?php

namespace App\Utils;


use DateTime;

class GroupDateHelper
{

	private $groupDate;
	private $dateTime;

	public function newGroup(DateTime $dateTime) : bool {
		$entityDate = $dateTime->format('Y-m-d');
		$currentTimeZone = new \DateTimeZone(getenv('TIMEZONE'));
		$currentDateTime = (new DateTime())->setTimezone($currentTimeZone);
		$currentDate = $currentDateTime->format('Y-m-d');

		if(!empty($this->groupDate) && $entityDate === $this->groupDate){
			return false;
		}
		$this->groupDate = $entityDate;
		switch (true){
			case $entityDate === $currentDate:
				$this->dateTime = 'Сегодня, ' . $entityDate;
				break;
			case $entityDate === $currentDateTime->modify('-1 day')->format('Y-m-d');
				$this->dateTime = 'Вчера, ' . $entityDate;
				break;
			default:
				$this->dateTime = $entityDate;
		}
		return true;
	}

	public function getDateTime() : string {
		return $this->dateTime;
	}
}