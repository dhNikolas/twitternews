<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Tag;
use App\Service\TwitterApiClient;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetNews extends Command
{
	/**
	 * @var TwitterApiClient
	 */
	protected $twitterClient;

	/**
	 * @var EntityManagerInterface
	 */
	protected $em;

	public function __construct(TwitterApiClient $apiClient, EntityManagerInterface $em)
	{
		$this->em = $em;
		$this->twitterClient = $apiClient;
		parent::__construct();
	}

	protected function configure()
	{
		$this->setName('app:get-news')
			->setDescription('Get news from twitter')
			->setHelp('Request news from Twitter');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$tags = $this->em->getRepository(Tag::class);
		/**
		 * Получаем последнюю статью
		 * @var $lastEntity Article
		 */
		$lastEntity = $this->em->createQueryBuilder()
			->select('a')
			->from(Article::class, 'a')
			->orderBy('a.created_at', 'DESC')
			->setMaxResults(1)
			->getQuery()
			->getOneOrNullResult();

		if(isset($lastEntity) && !empty($lastEntity->getTweetId())){
			$tweets = $this->twitterClient->getTwitSinceId($lastEntity->getTweetId());
		} else {
			$tweets = $this->twitterClient->getTwitSince('2018-06-25');
		}

		$output->writeln('Fetch article from twitter - ' . count($tweets));
		foreach ($tweets as $tweet){
			if(!empty($tweet['full_text']) && !empty($tweet['full_text']) && !empty($tweet['created_at'])) {

				$article = new Article();
				$article->setText($tweet['full_text']);
				$article->setTweetId($tweet['id_str']);
				$article->setCreatedAt(new DateTime($tweet['created_at']));

				foreach ($tweet['entities']['hashtags'] as $hashtag){
					$currentTag = $tags->findOneBy(['name'=> $hashtag['text']]);
					if(empty($currentTag)){
						$currentTag = new Tag();
						$currentTag->setName($hashtag['text']);
						$currentTag->setCreatedAt(new DateTime('now'));
						$this->em->persist($currentTag);
					}
					$article->addTag($currentTag);
				}
				$this->em->persist($article);
				$this->em->flush();

			} else {
				$output->writeln('Wrong data for Article');
			}

		}
	}

}