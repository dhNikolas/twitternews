<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getArticles(int $page = 1, string $queryString = '') : Pagerfanta
	{
		$queryBuilder = $this->createQueryBuilder('a')->orderBy('a.created_at', 'DESC');

		if(!empty($queryString)){
			foreach ($this->searchQuery($queryString) as $key => $term) {
				$queryBuilder
					->orWhere('a.text LIKE :t_'.$key)
					->setParameter('t_'.$key, '%'.$term.'%')
				;
			}
		}

		$paginator = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
		$paginator->setMaxPerPage(15);
		$paginator->setCurrentPage($page);
		return $paginator;
	}

	public function getPagenatorView(Pagerfanta $pagenator, string $queryString): string
	{
		$pageGenerator = function ($page) use ($queryString){
			$url = '?page=' . $page;
			if(!empty($queryString)){
				$url .= '&q=' . $queryString;
			}
			return $url;
		};
		$view  = new DefaultView();
		return $view->render($pagenator,$pageGenerator);
	}

	private function searchQuery(string $query) : array {

		$query = $this->sanitizeSearchQuery($query);
		$searchTerms = $this->extractSearchTerms($query);
		if (0 === count($searchTerms)) {
			return [];
		}
		return $searchTerms;
	}

	private function sanitizeSearchQuery(string $query): string
	{
		return trim(preg_replace('/[[:space:]]+/', ' ', $query));
	}

	private function extractSearchTerms(string $searchQuery): array
	{
		$terms = array_unique(explode(' ', $searchQuery));
		return array_filter($terms, function ($term) {
			return 2 <= mb_strlen($term);
		});
	}
}
