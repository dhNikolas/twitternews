<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Tag;
use App\Repository\ArticleRepository;
use App\Repository\TagRepository;
use App\Utils\GroupDateHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
	/**
	 * @Route("/", name="main",  methods={"GET"})
	 * @param Request $request
	 * @param ArticleRepository $articlesRep
	 * @param TagRepository $tags
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function index(Request $request, ArticleRepository $articlesRep, TagRepository $tags)
    {
    	$page = $request->query->get('page') ?? 1;
    	$search = (string)$request->query->get('q') ?? '';

		$articles = $articlesRep->getArticles($page, $search);
		$paginator = $articlesRep->getPagenatorView($articles, $search);
		$mostPopularTags = $tags->getMostPopularTags();

        return $this->render('main/articles.html.twig',
			[
				'tags' => $mostPopularTags,
				'articles' => $articles,
				'query' => $search,
				'pagenatorView' => $paginator,
				'groupDateHelper' => new GroupDateHelper()
			]
		);
    }

	/**
	 * @Route("/tag/{tag}", name="by_tag")
	 * @param $tag
	 * @param TagRepository $tags
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function byTag($tag, TagRepository $tags){
		/**
		 * @var Tag $currentTag
		 */
		$currentTag = $this->getDoctrine()->getRepository(Tag::class)->find($tag);

		$articles = $currentTag->getArticles();
		$mostPopularTags = $tags->getMostPopularTags();
		return $this->render('main/tags.html.twig',
			[
				'tag'=> $currentTag,
				'articles' => $articles,
				'tags' => $mostPopularTags,
				'groupDateHelper' => new GroupDateHelper()
			]);
	}
}
