<?php
namespace App\Service;

use App\Service\Interfaces\TwitterApiClientInterface;
use App\Service\Exceptions\TwitterApiException;
use TwitterAPIExchange;

class TwitterApiClient implements TwitterApiClientInterface
{
	/**
	 * @var TwitterAPIExchange
	 */
	protected $_apiClient;

	/**
	 * @var array
	 */
	protected $_apiConfig;

	public function __construct()
	{
		$this->_apiConfig = [
			'oauth_access_token' => self::ACCESS_TOKEN,
			'oauth_access_token_secret' => self::ACCESS_TOKEN_SECRET,
			'consumer_key' => self::CONSUMER_KEY,
			'consumer_secret' => self::CONSUMER_SECRET
		];
		$this->_apiClient = new TwitterAPIExchange($this->_apiConfig);
	}

	/**
	 * @param string $id
	 * @return array
	 */
	public function getTwitById(string $id) : array {
		$url = self::API_URL . self::API_URL_SHOW;
		$getfield = '?id=' . $id;
		return $this->request($url, $getfield);
	}

	/**
	 * @param $user
	 * @param $since
	 * @return array
	 */
	public function getTwitSince($since, $user = self::DEFAULT_ACCOUNT): array {
		$url = self::API_URL . self::API_URL_SEARCH;
		$getfield = '?q=from:' . $user . '+since:' . $since;
		return $this->requestWithPagination($url, $getfield);
	}

	public function getTwitSinceId($sinceId, $user = self::DEFAULT_ACCOUNT){
		$url = self::API_URL . self::API_URL_SEARCH;
		$getfield = '?q=from:' . $user . '&since_id=' . $sinceId;
		return $this->requestWithPagination($url, $getfield);
	}

	/**
	 * @param string $url
	 * @param string $fields
	 * @param string $method
	 * @return array
	 */
	private function request (string $url, string $fields, string $method = self::METHOD_GET) : array {
		$response = [];
		$fields = $fields . '&tweet_mode=extended&count=100';
		var_dump($fields);
		try{
			$response = $this->_apiClient->setGetfield($fields)
				->buildOauth($url, $method)
				->performRequest();
			$response = json_decode($response, TRUE);
		}catch (\Exception $exception){
			throw new TwitterApiException();
		}
		return $response;
	}

	private function requestWithPagination($url, $fields){
		$nextResult = $fields;
		$statuses = [];
		do{
			$result = $this->request($url, $nextResult);
			$nextResult = NULL;
			if(!empty($result['statuses'])){
				$statuses = array_merge($statuses, $result['statuses']);
			}
			if(!empty($result['search_metadata']['next_results'])){
				$nextResult = urldecode($result['search_metadata']['next_results']);
			}
		} while (!empty($nextResult));

		return $statuses;
	}

}